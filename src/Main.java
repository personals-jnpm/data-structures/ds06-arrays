import controller.ArrayController;
import controller.StudentController;

public class Main {

    public static void main(String[] args) {
        ArrayController array = new ArrayController();
        StudentController students = new StudentController();

        array.printArray();
        System.out.println("El número mayor es: " + array.maxNumber());
        System.out.println("El número menor es: " + array.minNumber());
        System.out.println("El promedio es de: " + array.getAverage());

        System.out.println("   ");
        array.printArrayChar();
        System.out.println("El caracter mayor es: " + array.maxChar());
        System.out.println("El caracter menor es: " + array.minChar());

        System.out.println("   ");
        array.printArrayString();
        System.out.println("La cadena mayor es: " + array.maxString());
        System.out.println("La cadena menor es: " + array.minString());

        System.out.println("\n----------Students----------");
        students.printStudents();
        double[] maxGrade = students.maxStudentGrade();
        double[] minGrade = students.minStudentGrade();

        System.out.println("La nota máxima es de: " + maxGrade[0] + ", por el estudiante: " +
                           students.getStudentById((int) maxGrade[1]).getName());
        System.out.println("La nota mínima es de: " + minGrade[0] + ", por el estudiante: " +
                students.getStudentById((int) minGrade[1]).getName());
        System.out.println("El promedio de la nota de los estudiantes es de: "+
                Math.round((students.getAverageGrade()) * 100.0) / 100.0);

    }

}
