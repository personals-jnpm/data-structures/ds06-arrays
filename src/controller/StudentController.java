package controller;

import model.Student;

public class StudentController {

    private final int NUMBER_STUDENTS = 10;
    private final Student[] students;

    public StudentController() {
        students = new Student[NUMBER_STUDENTS];
        initStudents();
    }

    private void initStudents() {
        String[] names = {"Andrea", "David", "Juan", "Barry", "Carlos", "Andrés", "Nicolás", "Camila", "Alejandra",
                "Sofía"};
        for (int i = 0; i < NUMBER_STUDENTS; i++) {
            double grade = Math.round((Math.random() * (5 - 1) + 1) * 100.0) / 100.0;
            students[i] = new Student(i, names[i], grade);
        }
    }

    public double[] maxStudentGrade() {
        double[] maxGrade = new double[2];

        for (int i = 0; i < NUMBER_STUDENTS; i++) {
            if (i == 0) {
                maxGrade[0] = students[i].getGrade();
                maxGrade[1] = i;
            } else if (maxGrade[0] < students[i].getGrade()) {
                maxGrade[0] = students[i].getGrade();
                maxGrade[1] = i;
            }
        }
        return maxGrade;
    }

    public double[] minStudentGrade() {
        double[] minGrade = new double[2];

        for (int i = 0; i < NUMBER_STUDENTS; i++) {
            if (i == 0) {
                minGrade[0] = students[i].getGrade();
                minGrade[1] = i;
            } else if (minGrade[0] > students[i].getGrade()) {
                minGrade[0] = students[i].getGrade();
                minGrade[1] = i;
            }
        }
        return minGrade;
    }

    public double getAverageGrade(){
        double sum = 0;
        for (Student student: students) {
           sum += student.getGrade();
        }
        return sum / NUMBER_STUDENTS;
    }

    public Student getStudentById(int id) {
        for (Student student : students) {
            if (student.getIdStudent() == id) {
                return student;
            }
        }
        return null;
    }

    public void printStudents() {
        System.out.print("[");
        for (Student student : students) {
            System.out.print(student.toString() + " \n");
        }
        System.out.print("]\n");
    }
}
