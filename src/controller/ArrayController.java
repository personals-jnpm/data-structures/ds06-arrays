package controller;

public class ArrayController {

    private final int LIMIT_NUMBERS = 10;
    private final int LIMIT_CHARS = 10;
    private final int LIMIT_CHAINS = 4;

    private final int[] numbers;
    private char[] chars;
    private String[] chains;

    public ArrayController() {
        numbers = new int[LIMIT_NUMBERS];
        chars = new char[LIMIT_CHARS];
        chains = new String[LIMIT_CHAINS];

        initNumbersArray();
        initCharsArray();
        initChainsArray();
    }

    private void initNumbersArray() {
        for (int i = 0; i < LIMIT_NUMBERS; i++) {
            numbers[i] = 1 + (int) (Math.random() * 100);
        }
    }

    private void initCharsArray() {
        chars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'z', 'f'};
    }

    private void initChainsArray(){
        chains = new String[]{"hola", "carro", "camisa", "ojo"};
    }

    public int maxNumber() {
        int maxNumber = 0;
        for (int i = 0; i < LIMIT_NUMBERS; i++) {
            if (i == 0) {
                maxNumber = numbers[i];
            } else if (maxNumber < numbers[i]) {
                maxNumber = numbers[i];
            }
        }
        return maxNumber;
    }

    public int minNumber() {
        int minNumber = 0;
        for (int i = 0; i < LIMIT_NUMBERS; i++) {
            if (i == 0) {
                minNumber = numbers[i];
            } else if (minNumber > numbers[i]) {
                minNumber = numbers[i];
            }
        }
        return minNumber;
    }

    public double getAverage(){
        double sum = 0;
        for (int i = 0; i < LIMIT_NUMBERS; i++) {
            sum += numbers[i];
        }
        return sum / LIMIT_NUMBERS;
    }

    public char maxChar() {
        char maxChar = 0;
        for (int i = 0; i < LIMIT_CHARS; i++) {
            if (i == 0) {
                maxChar = chars[i];
            } else if (maxChar < chars[i]) {
                maxChar = chars[i];
            }
        }
        return maxChar;
    }

    public char minChar() {
        char minChar = 0;
        for (int i = 0; i < LIMIT_CHARS; i++) {
            if (i == 0) {
                minChar = chars[i];
            } else if (minChar > chars[i]) {
                minChar = chars[i];
            }
        }
        return minChar;
    }

    public String maxString() {
        String maxString = "";
        for (int i = 0; i < LIMIT_CHAINS; i++) {
            if (i == 0) {
                maxString = chains[i];
            } else if (maxString.compareTo(chains[i]) > 0) {
                maxString = chains[i];
            }
        }
        return maxString;
    }

    public String minString() {
        String minString = "";
        for (int i = 0; i < LIMIT_CHAINS; i++) {
            if (i == 0) {
                minString = chains[i];
            } else if (minString.compareTo(chains[i]) < 0) {
                minString = chains[i];
            }
        }
        return minString;
    }

    public void printArray() {
        System.out.print("[");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
        System.out.print("]\n");
    }

    public void printArrayChar() {
        System.out.print("[");
        for (char character : chars) {
            System.out.print(character + " ");
        }
        System.out.print("]\n");
    }

    public void printArrayString() {
        System.out.print("[");
        for (String chain : chains) {
            System.out.print(chain + " ");
        }
        System.out.print("]\n");
    }
}
