package model;

public class Student {

    private int idStudent;
    private String name;
    private double grade;

    public Student() {
    }

    public Student(int idStudent, String name, double grade) {
        this.idStudent = idStudent;
        this.name = name;
        this.grade = grade;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "nombre = " + name +
               ", nota = " + grade;
    }
}
